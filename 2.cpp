// 1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\library\logic.h"
#include "iostream"

const char *msgs[] = { "0. Quit", "1. Get", "2. Set", "3. Change", "4. Change connect", "5. New terminal", "6. Initialization" }; // ������ �����������
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]); // ���������� �����������
int pr = 0;
const char *errmsgs[] = { "Tablitsa zapolnena", "Ok" };

int dialog(const char *msgs[], int);
int D_Get(logic &),
D_Set(logic &),
D_Change(logic &),
D_ChangeConnect(logic &v),
D_Add(logic &v),
D_Init(logic &);
int(*fptr[])(logic &) = { NULL, D_Get, D_Set, D_Change, D_ChangeConnect, D_Add, D_Init };

int getnum(int &);
int outall(logic &v);
int outone(terminal &p);
terminal* array();


int _tmain(int argc, _TCHAR* argv[])
{
	try{
		logic t;
		int rc;
		while (rc = dialog(msgs, NMsgs))
			if (!fptr[rc](t))
				break;
		printf("That's all. Bye!\n");
	}
	catch (const char *msg ) {
		std::cout << msg << std::endl;
		return 1;
	}
	return 0;
}

int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		puts(errmsg);
		errmsg = "You are wrong. Repeat, please!";
		// ����� ������ ����������� 
		for (i = 0; i < N; ++i)
			puts(msgs[i]);
		puts("Make your choice: --> ");
		n = getnum(rc); // ���� ������ ������������ 
		//ignore();
		std::cin.clear();
		_flushall();
		if (n == 0) // ����� ����� - ����� ������ 
			rc = 0;
	} while (rc < 0 || rc >= N);
	return rc;
}



int getnum(int &a){

	std::cin >> a;					//���� ������ �����			
	if (std::cin.eof())
		return 0;
	if (!std::cin.good()){			//����� ������
		return -1;
	}
	return 1;
}

int outall(logic &v){
	terminal p;
	std::cout << "NUNBER  TYPE  CONNECT  SIGNAL" << std::endl;
	for (int i = 0,k=0; i < v.getactualsize(); i++){
		k = i + 1;
		//p = v.get(k);
		p = v[k];
		std::cout << k;
		outone(p);
	}
	return 1;
}

int outone(terminal &p){
	std::cout << "       " << p.type << "     " << p.connect << "        " << p.signal << std::endl;
	return 1;
}

terminal* array(int &l){
	terminal *t;
	
	std::cout << "Enter terminal's number:";
	getnum(l);
	t=new terminal[l];
	for (int i = 0; i < l; i++){
		std::cout << "Enter type: ";
		getnum(t[i].type);
		std::cout << std::endl;
		std::cout << "Enter connect: ";
		getnum(t[i].connect);
		std::cout << std::endl;
		std::cout << "Enter signal: ";
		getnum(t[i].signal);
		std::cout << std::endl;
	}
	return t;
}



int D_Get(logic &v){ //
	int k,n;
	terminal p;
	//k = v.get();
	std::cout << "You want get 1(1) or all(0)?"<<std::endl;
	getnum (k);
	if (k == 1){
		std::cout << "Enter terminal's number: ";
		getnum(n);
		try{
			//p = v.get(n);
			p = v[n];
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
			return 1;
		}
		std::cout << "NUNBER  TYPE  CONNECT  SIGNAL" << std::endl;
		std::cout << n;
		outone(p);
	}
	if (k == 0){
		//v.out(std::cout);
		std::cout << v;
		//outall(v);
	}
	if ((k != 0) && (k != 1)){ std::cout << "Error. Try again."; }
	return 1;
}

int D_Set(logic &v){
	int k;
	std::cout << "You want set 1(1) or all(0)?" << std::endl;
	getnum(k);
	if (k == 1){
		D_Add(v);
	}
	if (k == 0){

		/*std::cout << "Enter array(type, connect, signal): ";
		try{
			v.set(std::cin);
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
			return 1;
		}*/
		terminal * u;
		int l;
		u=array(l);
		try{
			v = logic(u, l);
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
			return 1;
		}
		
	}
	if ((k != 0) && (k != 1)){ std::cout << "Error. Try again."; }
	return 1;
}

int D_Change(logic &v){
	int k,j;
	terminal t;
	std::cout << "You want change 1(1) or all(0)?" << std::endl;
	getnum(k);
	if (k == 1){
		std::cout << "Enter terminal's number: ";
		getnum(j);
		std::cout << std::endl;
		std::cout << "Enter type: ";
		getnum(t.type);
		std::cout << std::endl;
		std::cout << "Enter connect's number: ";
		getnum(t.connect);
		std::cout << std::endl;
		std::cout << "Enter signal: ";
		getnum(t.signal);
		std::cout << std::endl;
		try{
			v.set(j, t);
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
			return 1;
		}
	}
	if (k == 0){
		std::cout << "Enter array(type, connect, signal): ";
		try{
			std::cin>>v;
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
		}
	}
	if ((k!=0)&&(k!=1)){ std::cout << "Error. Try again."; }
	return 1;
}

int D_ChangeConnect(logic &v){
	int c, n;
	std::cout << "Enter terminal's number: ";
	getnum(n);
	std::cout << std::endl;
	//std::cout << "Enter connect's number: ";
	//getnum(c);
	std::cout << "Do you want +(1) or -(0)? ";
	getnum(c);

	if (c == 1){
		try{
			++v[n];
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
			return 1;
		}
	}

	if (c == 0){
		try{
			--v[n];
		}
		catch (const char *msg) {
			std::cout << msg << std::endl;
			return 1;
		}
	}

	if ((c != 1) && (c != 0)){
		std::cout << "You are wrong.";
		return 1;
	}
	std::cout << std::endl;
	/*try{
		v.set(c, n);
	}
	catch (const char *msg) {
		std::cout << msg << std::endl;
		return 1;
	}*/
	return 1;
}

int D_Add(logic &v){
	terminal t;
	
	std::cout << std::endl;
	std::cout << "Enter type: ";
	getnum(t.type);
	std::cout << std::endl;
	std::cout << "Enter connect's number: ";
	getnum(t.connect);
	std::cout << std::endl;
	std::cout << "Enter signal: ";
	getnum(t.signal);
	std::cout << std::endl;
	try{
		//v.set( t);
		v += t;
	}
	catch (const char *msg) {
		std::cout << msg << std::endl;
	}
	return 1;
}

int D_Init(logic &v){
	int a, b,s;
	terminal *d;

	std::cout << std::endl << "1" << std::endl;
	logic l;
	std::cout << l;
	std::cout << std::endl;

	std::cout << std::endl << "2" << std::endl;
	std::cout << "Input terminal's number" << std::endl;
	std::cin >> a;
	std::cout << "Output terminal's number" << std::endl;
	std::cin >> b;
	try{
		logic p(a, b);
		std::cout << p;
	}
	catch (const char *msg) {
		std::cout << msg << std::endl;
	}


	std::cout << std::endl << "3" << std::endl;
	std::cout << std::endl;
	d = array(s);
	try{
		logic m(d, s);
		std::cout << m;
	}
	catch (const char *msg) {
		std::cout << msg << std::endl;
	}
	return 1;
}

